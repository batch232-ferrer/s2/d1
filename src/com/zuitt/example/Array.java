package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Array {
    public static void main(String[] args){
    /*
        Arrays in Java
        -In Java, arrays are significantly rigid/strict. Even before filling in the arrays, we should already identify the data type and the size of the array.

        Syntax:
        dataType[] identifier = new dataType[numberOfElements]
        dataType[] identifier = {elementA, elementB, elementC...}

    */
        String[] newArr = new String[3];
        // toString() -> used to show the values of the array as a string
        System.out.println(Arrays.toString(newArr));
        // System.out.println(newArr); //result -> memory address

        newArr[0] = "Clark";
        System.out.println(Arrays.toString(newArr));
        newArr[1] = "Bruce";
        newArr[2] = "Lois";
        System.out.println(Arrays.toString(newArr));
        // newArr[3] = "Barry"; -> out of bounds for the number of elements in newArr
        // newArr[3] = 25; -> incorrect type of data type provided

        String[] arrSample = {"Tony", "Steve", "Thor"};
        System.out.println(Arrays.toString(arrSample));
        //arrSample[3] = "Peter"; -> out of bounds for the number of elements in arrSample

        Integer[] intArr = new Integer[5];
        intArr[0] = 54;
        intArr[1] = 23;
        intArr[2] = 25;
        intArr[3] = 30;
        intArr[4] = 62;
        System.out.println("Initial order of the int Arr: " + Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Order of items after sort: " + Arrays.toString(intArr));

        //Arrays.sort(originalArr, fromIndex, endIndex)
        Arrays.sort(intArr, Collections.reverseOrder());
        System.out.println("Order of items after sort: " + Arrays.toString(intArr));

        //ArrayList
        /*
            Array Lists are resizable collections/arrays that function similarly to how arrays work in JS.
            Syntax: ArrayList<dataType> identifier = new ArrayList<>();
        */

        ArrayList<String> students = new ArrayList<>();
        // Array List methods
        // arrayListName.add(itemToAdd) - adds elements in our array list
        students.add("Paul");
        students.add("John");
        System.out.println(students);

        // arrayListName.get(index) -> retrieve items from the array list using its index
        System.out.println(students.get(1));

        // arrayListName.set(index, value) - update an item by its index
        students.set(0, "George");
        System.out.println(students);

        // arrayListName.remove(index) - removes an item by its index
        students.remove(1);
        System.out.println(students);

        students.add("Ringo");
        System.out.println(students);

        // arrayListName.clear(); - clears out items in the array list.
        students.clear();
        System.out.println(students);

        // arrayListName.size(); - gets the length of array list.
        students.add("Wanda");
        students.add("Captain Marvel");
        students.add("Wasp");
        System.out.println(students.size());

        // ArrayList with initialized values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates", "Elon Musk", "Jeff Bezos"));
        System.out.println(employees);
        employees.add("Lucio Tan");
        System.out.println(employees);

        employees.add(2, "Henry Sy");
        System.out.println(employees);

        // Hashmaps
        // Most objects in Java are defined and are instantiations of Classes that contains a proper set of properties and methods. There might be use of cases where this is not appropriate, or you may simply want to store a collection of data in key-value pairs.
        // In Java "keys" are also referred to as "fields".
        // Syntax: HashMap<fieldDataType, valueDataType> identifier = new HashMap<>();

        HashMap<String, String> userRoles = new HashMap<>();
        // add new field and values in the hashmap
        // hashMapName.put(<field>, <value>);

        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "Mage");
        System.out.println(userRoles);
        userRoles.put("Alice", "Marksman");
        System.out.println(userRoles);
        userRoles.put("Dennis", "Tank");
        System.out.println(userRoles);

        // retrieve values by field
        // hashMapsName.get("field");
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Anna"));
        System.out.println(userRoles.get("Dennis"));
        System.out.println(userRoles.get("dennis")); //case-sensitivity will result to null
        System.out.println(userRoles.get("Ellen")); //non-existing field will result to null

        // removes an element / field-value
        // hashMapsName.remove("field");
        userRoles.remove("Anna");
        System.out.println(userRoles);

        // retrieve hashMap keys
        // hashMapName.keySet();
        System.out.println(userRoles.keySet());

    }

}
