package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner fruitScanner = new Scanner(System.in);

        // [PART 1] Array
        String[] fruitArr = {"apple", "avocado", "banana", "kiwi", "orange"};

        System.out.println("Which fruit would you like to get the index of:");
        String fruit = fruitScanner.nextLine();

        System.out.println("The index of " + fruit + " is: " + Arrays.asList(fruitArr).indexOf(fruit));

        // [PART 2] ArrayList
        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Evian", "Ramil", "John", "Patricia"));
        System.out.println("My friends are: " + friends);

        // [PART 3] Hashmap
        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
